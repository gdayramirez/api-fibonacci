var bigInt = require("big-integer");
/**
 * 
 * @param {Number} n 
 * @returns {String} fibonacci number
 * @description recibe un indice y obtiene el numero de fibonacci, 
 * este es retornado como string para poder convertirlo a bigInt
 */
exports.fibonacci = (index) =>{
    var init = [];
    init[0] = 1;
    init[1] = 1;
    if (index == 1 && index == 2) return 1;
    for (var i = 2; i < index; i++){
        init[i] = init[i-1] + init[i-2];
    }
    return init[index-1]+""
};

exports.init = (req, res, next) => {
    console.log(req.body)
    let { number } = req.body;
    if (!number) return res.send(400);
    if (typeof number != "number") return res.send(400);
    /**
     * !!! SUPPORT 1476 SEQUENCE
     */
    if( number > 1476) return res.send(400)
    let found = this.fibonacci(parseInt(number));
    res.send({
        message: 'SUCCESS',
        code: 200,
        result: found,
        fibonacci : bigInt(found)
    });
};