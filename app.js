'use strict';
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
/**
 * Import Routes
 */
const indexRoutes = require('./routes');
/**
 * Using Routes
 */
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', indexRoutes);
/**
 * EXPORTS
 */
module.exports = app;
