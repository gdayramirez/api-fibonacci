'use strict';
const api = require('express').Router();
const fibonacciController = require('../controllers/fibonacci')
module.exports = (()=>{
    
    api.get('/',(req,res)=>res.send({
        message : 'API IS RUNNING',
        code : 200
    }))
    
    api.all('*', function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        next();
    });

    api.post('/fib',fibonacciController.init)
    
    return api;
})();