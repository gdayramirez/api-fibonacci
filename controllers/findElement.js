
/**
 * @param {Array} array 
 * @param {Number} index 
 * @description Buscar un elemento dentro de un arreglo
 * @returns {Number} si elemento entonces indice, si no -1
 */
exports.findIndexFast  = (array,index)=>{
    let myIndex = {}
    for(var i = 0; i <= array.length; i++) myIndex[array[i]] = i
    return myIndex[index] ? myIndex[index] : -1
}

